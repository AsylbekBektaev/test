<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin');
});

Route::post('/auth', 'AsylController@auth')->name('auth');

Route::get('/update/{id}', 'AsylController@update')->name('update');

Route::post('/update_new', 'AsylController@new_data')->name('new_data');

Route::get('/delete/{id}', 'AsylController@fileDelete')->name('file.delete');

Route::get('/', function () {
    return view('admin');
})->name('exit');

Route::get('/auth', 'AsylController@back')->name('back');

Route::get('/select', 'AsylController@select')->name('select');
