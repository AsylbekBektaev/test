@extends('temp')
@section('title')
    Таблица
@endsection
@section('body')
    @if(session()->has('update') ?? "")
        <div class="message">
            {{'ПОСЛЕДНИЙ ОБНОВЛЕННЫЙ  ID : '. session('update')}}
        </div>
    @endif
    @if(session()->has('delete') ?? "")
        <div class="message_2">
            {{'  ПОСЛЕДНИЙ УДАЛЕННЫЙ ID : '. session('delete')}}
        </div>
    @endif

    @if($table)
        @include('exit')

        <table>
            @if(session()->has('name'))

                <tr>
                    <div class="name_table">
                        {{'таблица  :  ' . session('name')}}
                    </div>
                </tr>
            @endif
            <tr>
                <th>ID</th>
                <th>TITLE</th>
                <th>DESCRIPTION</th>
                <th>IMAGE</th>
                <th>ACTIVE</th>
                <th>CREATED_AT</th>
                <th>CUSTOMER_ID</th>
                <th>UPDATE</th>
                <th>DELETE</th>
            </tr>

            @foreach($table as $val)
                <tr>
                    <td>{{$val->id}}</td>
                    <td>{{$val->title}}</td>
                    <td>{{$val->description}}</td>
                    <td>
                        <img width="180px" height="180px" src="http://indigo24.xyz/uploads/tapes/{{$val->image}}">
                        <video autoplay="true" loop="true" muted="muted"
                               src="http://indigo24.xyz/uploads/tapes/{{$val->image}}"></video>
                    </td>
                    <td>{{$val->active}}</td>
                    <td>{{$val->created_at}}</td>
                    <td>{{$val->customer_id}}</td>
                    <td>
                        <a href="{{route('update',$val->id)}}">
                            <button>Обновить</button>
                        </a>
                    </td>
                    <td>
                        <a href="{{route('file.delete',$val->id)}}">
                            <button>Удалить</button>
                        </a></td>
                </tr>
            @endforeach
        </table>
        <ul>
            <li>{{$table->links()}}</li>
        </ul>
    @endif
@endsection
<style>
   html,body, video {
        width: 100%;
        height: auto;
    }

    html ,body ,img {
        width: 100%;
        height: auto;
    }

    table {
        margin: 1% 0 0 7%;
        width: 88%;
    }

    .name_table {
        text-align: center;
        width: 100%;
        font-size: 22px;
        font-weight: 800;
        padding-top: 140px;
    }

    .message {
        text-align: center;
        background-color: #cd5c5c;
        height: 40px;
        position: fixed;
        top: 50px;
        width: 100%;
        font-size: 25px;
        padding-top: 10px;
    }

    .message_2 {
        text-align: center;
        background-color: #cd5c5c;
        height: 40px;
        position: fixed;
        top: 0px;
        width: 100%;
        font-size: 25px;
        padding-top: 10px;
    }

    table :hover {
        background-color: darkgrey;
    }

    tr th {
        max-width: 170px;
        max-height: 20px;
        border: solid 1px grey;
    "
    }

    tr td {
        border: solid 1px grey;
        max-width: 100px;
        max-height: 20px;
        text-align: center;
        word-wrap: break-word;
    }

    ul {
        margin: 10px 0 0 0;
        width: 300px;
        display: flex;
    }

    ul li {
        margin: 10px 10px 10px 10px;
        text-decoration: none;
        list-style: none;
    }

    button {
        width: 80px;
        height: 30px;
    }
</style>
