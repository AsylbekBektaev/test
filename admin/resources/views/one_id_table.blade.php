@extends('temp')
@section('title')
    ОБНОВИТЬ Данные
@endsection
@section('body')
    @if($id_table)
        @include('exit')
        <a href="{{route('back')}}">
            <button class="but_back">
                назад
            </button>
        </a>
        <form action="{{route('new_data')}}" method="post">
            @csrf
            <label for="id">
                ID :
            </label>
            {{$id_table->id}}<br>
            <input type="hidden" value="{{$id_table->id}}" name="id">
            <label>
                Title:
            </label>
            <input type="text" value="{{$id_table->title}}" name="title"><br>
            <label>
                Description :
            </label>
            <input type="text" value="{{$id_table->description}}" name="description"><br>
            <label>
                Image :
            </label>
            <input type="text" value="{{$id_table->image}}" name="image"><br>
            <label>
                ACTIVE :
            </label>
            {{$id_table->active}}<br>
            <label>
                CREATED_AT :
            </label>
            {{$id_table->created_at}}<br>

            <label>
                Customer_id :
            </label>
            <input type="text" value="{{$id_table->customer_id}}" name="customer_id"><br>
            <button>Обновить</button>
        </form>
    @endif
@endsection
<style>
    .but_back {
        font-size: 28px;
        position: fixed;
        right: 30px;
        bottom: 90px;
        height: 40px;
        width: 100px;
        background-color: #cd5c5c;
        opacity: 0.7;
        color: white;
    }

    input {
        margin: 30px 0 0 30px;
        width: 400px;
        height: 50px;
    }

    form label {
        margin: 50px 0 10px 0;
    }

    form {
        flex-direction: column;
        width: 60%;
        margin: 10% 40% 0 40%;
        font-size: 22px;
    }

    button {
        width: 80px;
        height: 30px;
        margin: 30px 0 0 0;
    }
</style>
