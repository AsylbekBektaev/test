@extends('temp')
@section('title')
    Автаризация
@endsection
@section('body')
    <form action="{{route('auth')}}" method="post">
        @csrf
        <label>
            Login :
        </label>
        <input type="text" name="login"><br>
        <label>
            Password :
        </label>
        <input type="password" name="pas"><br>
        <button>Auth</button>
    </form>
@endsection
<style>
    form {
        position: absolute;
        left: 50%;
        top: 35%;
        transform: translate(-50%, -50%);
    }

    form input {
        margin-top: 30px;
    }

    button {
        margin-top: 30px;
    }
</style>
