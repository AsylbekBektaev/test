<?php

namespace App\Http\Controllers;

use App\Tape;
use Carbon\Carbon;

use http\Header;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AsylController extends Controller
{
    /**
     * @param Request $req
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function auth(Request $req)
    {
        if ($req->login === "yes" && $req->pas === "123") {
            $table = Tape::paginate(10);

            $name = new Tape();
            $name_2 = $name->name();
            session(['name' => $name_2]);


            return view('table_select', ['table' => $table]);
        } else {
            return view('admin');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id)
    {
        $id_table = Tape::find($id);
        return view('one_id_table', ['id_table' => $id_table]);
    }

    /**
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function new_data(Request $req)
    {
        $table_data = Tape::find($req->id);
        $table_data->id = $req->id;
        $table_data->title = $req->title;
        $table_data->description = $req->description;
        $table_data->image = $req->image;
        $table_data->customer_id = $req->customer_id;
        $table_data->save();
        session(['update' => $req->id]);

        return redirect()->route('select');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fileDelete($id)
    {

        session(['delete' => $id]);
        $image = Tape::find($id)->image;

        $path = (base_path() . "/uploads/tapes/" . $image);
        File::delete($path);

        Tape::where('id', $id)->delete();
        return redirect()->route('select');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function select()
    {
        $table = Tape::paginate(10);

        return view('table_select', ['table' => $table]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function back()
    {
        $table = Tape::paginate(10);

        return view('table_select', ['table' => $table]);

    }
}
