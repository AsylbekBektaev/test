<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Testing\Constraints\SoftDeletedInDatabase;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tape extends Model
{
    use SoftDeletes;

    protected $table = 'tapes';
    protected $dates = ['deleted_at'];

    public function name()
    {
        return $this->table;
    }
}
